from parallelMatrixMultiplication import paraMult
import matplotlib.pyplot as plt
import numpy as np
from mpi4py import MPI 

timeList = []
for i in range(50,1000,50):
    timeSum = 0
    for j in range(0,5):
        timeSum = timeSum + paraMult(i)
    timeList.append(timeSum/5)

if MPI.COMM_WORLD.Get_rank() == 0:
    f = open("paraMult%d.txt" % MPI.COMM_WORLD.Get_size(),"w")
    for i in timeList:
        f.write("%f\n" % i)
    f.close()

#plt.scatter(range(50,1000,50),timeList)
#plt.show()
