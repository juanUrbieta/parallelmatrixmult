from mpi4py import MPI
import numpy as np
import time

def paraMult(size):
    comm = MPI.COMM_WORLD
    nodes = comm.Get_size()
    rank = comm.Get_rank()

    t_start = 0
    t_end = 0

    B = np.zeros((size,size))

    comm.Barrier() #sync nodes
    if rank == 0:
        A = np.random.rand(size,size)
        B = np.random.rand(size,size)

        print("A: ", A)
        print("B: ", B)

        t_start = time.time()

        for i in range(1,nodes):
            Arows = np.vsplit(A,size)
            for j in range(0,size):
                if i-1 == j % (nodes-1):
                    comm.send(Arows[j], dest=i, tag=i)
                    #print("Sent row ", j, " to node", i)
    
    comm.Barrier()
    comm.Bcast(B, root=0)
    
    if rank != 0:
        receives = int(size / (nodes-1)) + int(np.clip(size % (nodes-1) / rank, a_min = 0, a_max = 1))
        for i in range(0, receives):
            C = np.zeros((1,size))
            A = comm.recv(source=0, tag=rank)
            #print("Row ", rank-1 + i*(nodes-1), " Rank ", rank, ":", A)
            for j in range(0,size):
                C[0,j] = np.vdot(A[0], B[:,j])
            #print(A[0], B[:,0],C)
            comm.send(C, dest=0, tag=(rank-1)+i*(nodes-1))

    comm.Barrier()

    if rank == 0:
        C = np.zeros((size,size))
        for i in range(0, size):
            C[i,:] = comm.recv(tag=i)
        t_end = time.time()
        print("C: ", C)
        return t_end - t_start

    return 0
