import numpy as np
import time

def serMult(size):
    A = np.random.rand(size,size)
    B = np.random.rand(size,size)
    
    print("A: ",A)
    print("B: ",B)

    t_start = time.time()
    C = np.zeros((size,size))

    for r in range(0,size):
        for c in range(0,size):
            #print(A[r,:],B[:,c])
            C[r,c] = np.vdot(A[r,:],B[:,c])

    print("C: ",C)
    t_end = time.time()

    #print("A: ",A)
    #print("B: ",B)
    #print("C: ",C)

    return t_end-t_start
