import matplotlib.pyplot as plt
import numpy as np

s = open('serMult.txt','r')
ser = []
lines = []
val = s.readline()
while val:
    ser.append(float(val[0:-1]))
    val = s.readline()
lines.append(plt.plot(range(50,1000,50),ser))
s.close()

for i in range(2,7):
    p = open('paraMult%d.txt' % i,'r')
    para = []
    val = p.readline()
    while val:
        para.append(float(val[0:-1]))
        val = p.readline()
    lines.append(plt.plot(range(50,1000,50),para))
    p.close()

plt.legend(('Serial Mult.','2 Node Mult.','3 Node Mult.','4 Node Mult.','5 Node Mult.','6 Node Mult.'))
plt.title('Parallel vs. Serial Algorithm Execution Time')
plt.xlabel('Size of Matrix')
plt.ylabel('Execution Time (s)')
plt.show()
